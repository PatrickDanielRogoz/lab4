package rogoz.patrick.lab4.ex4;

class Author {
    String name;
    String email;
    char gender;
    public Author(String name,String email,char gender)
    {
        this.name=name;
        this.email=email;
        this.gender=gender;

    }

    String getName()
    {
        return name;
    }
    String getEmail()
    {
        return email;
    }

    char getGender()
    {
        return gender;
    }

    void setEmail(String email)
    {
        this.email=email;

    }

    String toString(String name,String email,char gender)
    {
        return name + " " + "(" + gender + ") at " + email;

    }

}
public class Book {
    String name;
    Author[] author;
    double price;
    int qtyInStock=0;
    public Book(String name , Author[] author, double price)
    {
        this.name=name;
        for (int i=0;i<author.length;i++) {
            this.author[i]=author[i];
        }
        this.price=price;

    }

    public Book(String name, Author[] author , double price, int qtyInStock)
    {
        this.name=name;
        for (int i=0;i<author.length;i++)
            this.author[i]=author[i];
        this.price=price;
        this.qtyInStock=qtyInStock;
    }

    String getName()
    {
        return name;
    }

    public Author[] getAuthors() {
        Author[] authors= new Author[10];
        for (int i=0;i<authors.length;i++)
            authors[i]=author[i];
        return authors;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }
     void printAuthors()
   {
       for (int i=0;i<author.length;i++)
           System.out.println(author[i].name+" ");
   }

    public String toString(String name, int n) {
        return "Book-name : '"+  name+"' by "+n+" authors";
    }

}