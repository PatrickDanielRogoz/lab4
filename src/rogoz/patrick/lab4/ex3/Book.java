package rogoz.patrick.lab4.ex3;
import rogoz.patrick.lab4.ex2.Author;

 class Author2 {
    String name;
    String email;
    char gender;
    public Author2(String name,String email,char gender)
    {
        this.name=name;
        this.email=email;
        this.gender=gender;

    }

    String getName()
    {
        return name;
    }
    String getEmail()
    {
        return email;
    }

    char getGender()
    {
        return gender;
    }

    void setEmail(String email)
    {
        this.email=email;

    }

    String toString(String name,String email,char gender)
    {
        return name + " " + "(" + gender + ") at " + email;

    }

}

public class Book {
    String name;
    Author2 author;
    double price;
    int qtyInStock=0;
    public Book(String name ,Author2 author,double price)
    {
         this.name=name;
         this.author=author;
         this.price=price;

    }

    public Book(String name, Author2 author ,double price,int qtyInStock)
    {
        this.name=name;
        this.author=author;
        this.price=price;
        this.qtyInStock=qtyInStock;
    }

    public Book(String name, Author rr, double price) {
    }

    String getName()
    {
        return name;
    }

    public Author2 getAuthor() {
        return author;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }


    public String toString(String name) {
        return "Book-name : '"+  name+"' by ";
    }

}
