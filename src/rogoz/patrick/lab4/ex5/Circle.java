package rogoz.patrick.lab4.ex5;

public class Circle {

    private double radius;
    private String color;

    public Circle()
    {
        radius=1.0;
        color="red";

    }

    public Circle(double radius)
    {
        this.radius=radius;

    }

    double getRadius()
    {

        return radius;
    }
    double getArea(double radius)
    {
        return Math.PI * (radius * radius);

    }


    static class Cylinder extends Circle{

        double heigth;
        public Cylinder()
        {
            heigth=1.0;
        }
        public Cylinder(double radius)
        {
            super(radius);
        }
        public  Cylinder(double radius, double heigth)
        {
            super(radius);
            this.heigth=heigth;
        }

        public double getHeigth() {
            return heigth;
        }

        public double getVolume()
        {
            return Math.PI*(super.radius*super.radius)*heigth;
        }


    }




}