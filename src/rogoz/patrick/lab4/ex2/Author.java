package rogoz.patrick.lab4.ex2;

public class Author {
    String name;
    String email;
    char gender;
    public Author(String name,String email,char gender)
    {
        this.name=name;
        this.email=email;
        this.gender=gender;

    }

    String getName()
    {
        return name;
    }
    String getEmail()
    {
        return email;
    }

   char getGender()
   {
       return gender;
   }
   
   void setEmail(String email)
   {
       this.email=email;
       
   }
   
    String toString(String name,String email,char gender)
   {
       return name + " " + "(" + gender + ") at " + email;
       
   }

}
