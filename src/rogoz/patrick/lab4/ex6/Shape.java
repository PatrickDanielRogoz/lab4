package rogoz.patrick.lab4.ex6;

public class Shape {


    String color;
    boolean filled;

    public Shape()
    {

        color="green";
        filled=true;
    }

    public Shape(String color,boolean filled)
    {
        this.color=color;
        this.filled=filled;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    @Override
    public String toString() {
        boolean aux=filled;
        String s;
        if (aux==true)
            s="filled";
        else s="not filled";
        return "Shape with color " + color +" and " + s;
    }
}


class Circle extends Shape {
    double radius;
public Circle() {
    radius=1.0;
}
public  Circle(double radius) {
    this.radius=radius;
}
public Circle(double radius,String color,boolean filled){
    this.radius=radius;
    this.color=color;
    this.filled=filled;
}

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea(){
        return Math.PI * (radius * radius);
    }

    public double getPerimeter(){
         return Math.PI*radius*2;
    }

    @Override
    public String toString() {
        return "Circle with radius="  + radius + " which is a subclass of "+super.toString();
    }
}

class Rectangle extends Shape {
    double width;
    double length;

    public Rectangle() {
        width = 1.0;
        length = 1.0;
    }

    public Rectangle(double width, double length) {
        this.width = width;
        this.length = length;
    }

    public Rectangle(double width, double length, String color, boolean filled) {
        this.width = width;
        this.length = length;
        this.color = color;
        this.filled = filled;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getArea() {
        return width * length;
    }

    public double getPerimeter() {
        return 2 * (width * length);
    }

    @Override
    public String toString() {
        return "Rectangle with width=" + width + " and length=" + length + " which is a subclass of " + super.toString();
    }
}

    class Square extends Rectangle{
        public Square(){
            width=1.0;
            length=1.0;
        }
        public Square(double side){
            this.length=side;
            this.width=side;
        }
        public Square(double width, double length,String color,boolean filled){
            this.width=width;
            this.length=length;
            this.color=color;
            this.filled=filled;
        }
        public double getSide(){
            return width;
        }
        public void setSide(double side){
            this.width=side;
            this.length=side;
        }

        @Override
        public void setLength(double length) {
            super.setLength(length);
        }

        @Override
        public void setWidth(double width) {
            super.setWidth(width);
        }

        @Override
        public String toString() {
            return "A Square with side="+width+" ,which is a sublcass of "+super.toString();
        }
    }

