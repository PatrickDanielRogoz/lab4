package rogoz.patrick.lab4.ex6;

public class Test {
    public static void main(String[] args)
    {
        Shape circle=new Shape("Red",false);
        System.out.println(circle.toString());
        Circle c=new Circle(5.0,"red",true);
        System.out.println(c.toString());
        Rectangle r=new Rectangle(2.5,4.3,"Red",false);
        System.out.println(r.toString());
        Square square=new Square(4.0,4.0,"Green",true);
        System.out.println(square.toString());
    }
}
