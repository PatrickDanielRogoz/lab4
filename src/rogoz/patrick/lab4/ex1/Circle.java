package rogoz.patrick.lab4.ex1;

public class Circle {

    private double radius;
    private String color;

    public Circle()
    {
        radius=1.0;
        color="red";

    }

    public Circle(double radius)
    {
        this.radius=radius;

    }

    double getRadius()
    {

        return radius;
    }
    double getArea(double radius)
    {
        return Math.PI * (radius * radius);

    }




}
